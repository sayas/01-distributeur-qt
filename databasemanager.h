#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QObject>
#include <QDebug>
#include <QFile>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

#define CONNECTION_NAME "MyConnection"
#define DATABASE "distributeur.sqlite3"

class DatabaseManager : public QObject
{
    Q_OBJECT
public:
    explicit DatabaseManager(QObject *parent = nullptr);

    QSqlDatabase createConnection();
    QSqlDatabase connection();

signals:

};

#endif // DATABASEMANAGER_H
