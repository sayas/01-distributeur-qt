#include "databasemanager.h"

DatabaseManager::DatabaseManager(QObject *parent) : QObject(parent)
{

}


QSqlDatabase DatabaseManager::createConnection() {
    QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE", CONNECTION_NAME);

    QFile file(DATABASE);

    if(file.exists()) {
        database.setDatabaseName(DATABASE);
        database.open();
    }
    else {
        database.setDatabaseName(DATABASE);
        database.open();

        QSqlQuery query(database);
        query.exec("create table product (id integer primary key autoincrement, name text)");
    }

    return database;
}


QSqlDatabase DatabaseManager::connection() {
    return QSqlDatabase::contains(CONNECTION_NAME) ? QSqlDatabase::database(CONNECTION_NAME) : createConnection();
}
