#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connection = DatabaseManager().connection();
    select();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::select() {
    // vider le tableau

    while(ui->tableWidget->rowCount() > 0) {
        ui->tableWidget->removeRow(0);
    }

    // selection des donnees dans la base

    QSqlQuery query(connection);
    query.exec("select * from product");

    while (query.next()) {
        QString id = query.value("id").toString();
        QString name = query.value("name").toString();

        ui->tableWidget->insertRow(0);
        ui->tableWidget->setItem(0, 0, new QTableWidgetItem(id));
        ui->tableWidget->setItem(0, 1, new QTableWidgetItem(name));
    }
}


void MainWindow::on_buttonAdd_clicked()
{
    // ouverture d'une boite de dialogue

    bool ok;
    QString name = QInputDialog::getText(this, "add product", "product", QLineEdit::Normal, "", &ok);

    if(ok && !name.isEmpty()) {
        QSqlQuery query(connection);
        query.prepare("insert into product (name) values (:name)");
        query.bindValue(":name", name);

        if(query.exec()) {
            qDebug() << "success";
            select();
        }
        else {
            qDebug() << query.lastError().text();
        }
    }
    else {
        // message d'erreur
    }
}


void MainWindow::on_buttonDelete_clicked()
{
    if(!ui->tableWidget->selectedItems().isEmpty()) {
        int btn = QMessageBox::question(this, "delete", "Delete this product");

        if(btn != QMessageBox::Yes)
            return;

        // selection de la valeur de l'id de la lige selectionne

        int row = ui->tableWidget->currentRow();
        QString id = ui->tableWidget->item(row, 0)->text();

        // suppression des donnees dans la base

        QSqlQuery query(connection);
        query.prepare("delete from product where id=:id");
        query.bindValue(":id", id);

        if(query.exec()) {
            qDebug() << "success";
            select();
        }
        else {
            qDebug() << query.lastError().text();
        }
    }
    else {
        qDebug() << "empty selection";
    }
}

